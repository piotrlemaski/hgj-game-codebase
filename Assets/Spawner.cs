﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour
{
    float timer = 3;
    float maxTimer = 1;
    float currentMaxTimer;

    void Update()
    {
        timer += Time.deltaTime;
        if(timer > currentMaxTimer && GameManager.instance.bacterias.Count < 15)
        {
            currentMaxTimer = maxTimer * Random.Range(0.75f, 1.25f);
            timer = 0;
            GameObject obj = GameManager.instance.InstantiateObj(GameManager.instance.enemyPrefab, Player.instance.transform.parent, 1);
            Enemy enemy = obj.GetComponent<Enemy>();
            enemy.pattern = Random.Range(0, 3);
            enemy.shape = Random.Range(0, 7);
            enemy.interceptPower = Random.Range(-1f, 1f);
            enemy.attitude = Random.Range(-0.5f, 1f);
            enemy.color = Random.Range(0, 4);
            enemy.tentacles = new bool[] { Random.Range(0, 2) == 0, Random.Range(0, 2) == 0 };
            obj.transform.localPosition = new Vector3( transform.localPosition.x > 0 ? -1000 : 1000, transform.localPosition.y > 0 ? -1000 : 1000, 0);
            GameManager.instance.bacterias.Add(enemy);
        }
    }
}
