﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnimLeg : MonoBehaviour
{
    public Image image;
    public Sprite[] sprites;
    float timer;
    float spriteTime = 0.08f;
    int sprite = 0;

    void Start()
    {
        image.sprite = sprites[sprite];
    }

    void Update()
    {
        timer += Time.deltaTime;
        if(timer > spriteTime)
        {
            timer = 0;
            sprite = (sprite + 1) % 4;
            image.sprite = sprites[sprite];
        }
    }
}
