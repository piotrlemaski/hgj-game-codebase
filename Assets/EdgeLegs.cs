﻿using UnityEngine;
using System.Collections;

public class EdgeLegs : MonoBehaviour
{
    public GameObject prefab;

    void Start()
    {
        SpawnPoint(3, 5);
    }

    void SpawnPoint(int shape, int num)
    {
        switch (shape)
        {
            case 0: //square
                float len = 0.8f;
                float len2 = 0.90f;
                float len3 = 0.87f;
                CreateLine(new Vector2(len3, len), new Vector2(-len3, len), num / 4);
                CreateLine(new Vector2(-len, len), new Vector2(-len, -len2), num / 4);
                CreateLine(new Vector2(len3, -len2), new Vector2(len3, len), num / 4);
                CreateLine(new Vector2(-len, -len2), new Vector2(len3, -len2), num / 4);
                //CreatePoint(i * (1f / num), 1);
                break;
            case 1:  //triangle
                CreateLine(new Vector2(0, 0.8f), new Vector2(-0.9f, -0.80f), num / 3);
                CreateLine(new Vector2(0.9f, -0.80f), new Vector2(0, 0.8f), num / 3);
                CreateLine(new Vector2(-0.9f, -0.80f), new Vector2(0.9f, -0.80f), num / 3);
                break;
            case 2: // circle
                for (float i = 0; i < num; i++)
                {
                    GameObject obj = CreatePoint(0.86f * Mathf.Sin(i / num * 2 * Mathf.PI), 0.86f * Mathf.Cos(i / num * 2 * Mathf.PI));
                    obj.transform.localEulerAngles = new Vector3(0, 0, -i / num * 360f);
                }
                break;
            case 3:
                CreateLine(new Vector2(0.85f, 0.6f), new Vector2(0.4f, -0.7f), num);
                CreateLine(new Vector2(0.4f, -0.65f), new Vector2(-0.9f, -0.65f), num);
                CreateLine(new Vector2(-0.9f, -0.65f), new Vector2(-0.3f, 0.7f), num);
                CreateLine(new Vector2(-0.3f, 0.6f), new Vector2(0.85f, 0.6f), num);
                break;
        }
    }

    GameObject CreatePoint(float x, float y)
    {
        GameObject obj = Instantiate(GameManager.instance.legPrefab);
        obj.SetActive(true);
        obj.transform.SetParent(transform);
        obj.transform.localScale = new Vector3(1, 1, 1);
        obj.transform.localPosition = new Vector3(x, y, 0);
        return obj;
    }

    void CreateLine(Vector2 v1, Vector2 v2, int num)
    {
        Vector2 distance = v2 - v1;
        for (float i = 1; i < num; i++)
        {
            Vector2 point = v1 + (distance / num * i);
            GameObject obj = CreatePoint(point.x, point.y);

            Vector2 diff = point.normalized;
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            obj.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
            //obj.transform.rotation = Quaternion.LookRotation(point);
        }
    }
}
