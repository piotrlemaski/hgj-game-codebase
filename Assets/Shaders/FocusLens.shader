﻿Shader "FocusLens" 
{
	Properties
	{
		_MainTex ("Base (RGB), Alpha(A)", 2D) = "white" {}
		_AlphaTex("MaskTexture", 2D) = "white" {}
	}

	SubShader 
	{
		LOD 100

		Pass 
		{
			ZTest Always
			Cull Off
			ZWrite Off
			Offset -1, -1
			Fog { Mode Off }
			ColorMask RGB
			Blend SrcAlpha OneMinusSrcAlpha
				
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform sampler2D _AlphaTex;

			uniform half _RampOffset;
			half4 _MainTex_ST;

			struct appdata_t
			{
				float4 vertex : POSITION;
				half4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};
 
			struct v2f
			{
				float4 vertex : POSITION;
				half4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 worldPos : TEXCOORD1;
			};

			v2f vert(appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color;
				o.texcoord = v.texcoord;
				o.worldPos = TRANSFORM_TEX(v.vertex.xy, _MainTex);
				return o;
			}


			fixed4 frag(v2f i) : SV_Target
			{
				half4 original = tex2D( _MainTex, i.texcoord ) * i.color;
				half4 alpha2 = tex2D( _AlphaTex, i.texcoord );

				float2 factor = abs(i.worldPos);
				float val = 1.0 - max(factor.x, factor.y);

				if (val < 0.0) original.a = 0.0;
				if (alpha2.a < original.a) original.a = alpha2.a;
				//original.a = alpha2.a;
				return original;
			}
			ENDCG
		}
	}

Fallback off

}
