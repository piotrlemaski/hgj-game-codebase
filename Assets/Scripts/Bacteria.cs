﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class Bacteria : MonoBehaviour
{
    public int shape;
    // public bool roundEdge;
    //public int edgeShape;
    //0 - hairs, 1 - blobs
    public bool[] tentacles;
    public bool hasBlobs;
    public int pattern; // 0 - none, 1 - cienkie, 2 - grube
    public int color;
    public MoveType moveType;
    public Image image;
    
    public Vector2 speed;
    public float moveTimer;
    public float speedMove;
    MoveStats moveStats;
    float zChange;
    bool rotateInUpdate = false;
    List<Transform> legs = new List<Transform>();
    public float scale;


    void Start()
    {
        image = GetComponent<Image>();
        scale = image.rectTransform.rect.height / 2;
        Refresh();        
    }

    public void BakteriaUpdate()
    {
        if(rotateInUpdate)
        {
            transform.localEulerAngles += new Vector3(0, 0, zChange);
            rotateInUpdate = false;
        }        
        transform.localPosition += new Vector3(speed.x, speed.y, 0);
        speed *= 1 - (moveStats.drag * Time.deltaTime);
        transform.localPosition = new Vector3(Mathf.Clamp(transform.localPosition.x, -2000, 2000), Mathf.Clamp(transform.localPosition.y, -1200, 1200), 0);
    }

    public void Refresh()
    {
        image.sprite = GameManager.instance.GetSprite(shape, color, tentacles);
        legs.Clear();
        foreach(Transform t in transform)
        {
            if(t != transform)
                Destroy(t.gameObject);
        }
        SpawnPoint(20);
        /*if(pattern > 0)
        {
            GameObject obj = GameManager.instance.InstantiateObj(GameManager.instance.paternPrefab, transform, 1);
            Image img = obj.GetComponent<Image>();
            img.sprite = GameManager.instance.GetPattern(shape, pattern - 1);
        }*/       
    }

    public void GoTo(Vector3 position, bool subtractPosiotion)
    {
        if(subtractPosiotion)
            position = position - transform.localPosition;
        rotateInUpdate = true;
        moveStats = GameManager.instance.GetMoveStatWithType(moveType);
        float positionRot;
        float rotZ;
        int rotDirection;
        Vector2 playerDirection;        
        Vector2 positionNormalized;
        moveTimer += Time.deltaTime;

        rotZ = transform.localEulerAngles.z % 360;
        if (rotZ < 0)
            rotZ += 360;        

        positionNormalized = position.normalized;
        float asin = (Mathf.Asin(positionNormalized.x) * 360f / 2 / Mathf.PI);
        //positionRot = (Mathf.Sign(positionNormalized.y) > 0 ? 1 : -1) * Mathf.Asin(positionNormalized.x) * 360f / 2 / Mathf.PI + (Mathf.Sign(positionNormalized.y) > 0 ? 0 : 360);
        positionRot = (Mathf.Sign(positionNormalized.y) > 0 ? asin : 180 - asin);
        if (positionRot < 0)
            positionRot += 360;
        positionRot = 360 - positionRot;

        float diff1 = positionRot - rotZ;
        if (diff1 < 0)
            diff1 += 360;
        float diff2 = rotZ - positionRot;
        if (diff2 < 0)
            diff2 += 360;

        rotDirection = diff1 < diff2 ? 1 : -1;
        //Debug.Log(positionRot + " " + rotZ + " " + rotDirection + " " + diff1 + " " + diff2);

        float maxRot = Mathf.Min(diff1,  diff2);
        zChange = Mathf.Clamp(rotDirection * 360f / moveStats.fullRotationTime * Time.deltaTime, -maxRot, maxRot);

        if (moveTimer > moveStats.timeBeetweenMovingForward)
        {
            playerDirection = new Vector2(-Mathf.Sin(2 * Mathf.PI * transform.localEulerAngles.z / 360f), Mathf.Cos(2 * Mathf.PI * transform.localEulerAngles.z / 360f));
            moveTimer = 0;
            speedMove = moveStats.movingSpeedPerSec * moveStats.timeBeetweenMovingForward;
            speed += positionNormalized * speedMove;
            PulseLegs();
        }
    }

    void SpawnPoint(int num)
    {
        Debug.Log("SpawnPoint " + name + " " + (!tentacles[0] && !tentacles[1]));
        if (!tentacles[0] && !tentacles[1])
            return;      

        int val = shape;
        if (val == 3)
            val = 0;
        if (val == 4)
            val = 1;
        if (val == 6)
            val = 5;

        switch (val)
        {
            case 0: //square
                float len = 0.8f;
                float len2 = 0.90f;
                float len3 = 0.87f;
                CreateLine(new Vector2(len3, len), new Vector2(-len3, len), num / 4);
                CreateLine(new Vector2(-len, len), new Vector2(-len, -len2), num / 4);
                CreateLine(new Vector2(len3, -len2), new Vector2(len3, len), num / 4);
                CreateLine(new Vector2(-len, -len2), new Vector2(len3, -len2), num / 4);
                //CreatePoint(i * (1f / num), 1);
                break;
            case 1:  //triangle
                CreateLine(new Vector2(0, 0.8f), new Vector2(-0.9f, -0.80f), num / 3);
                CreateLine(new Vector2(0.9f, -0.80f), new Vector2(0, 0.8f), num / 3);
                CreateLine(new Vector2(-0.9f, -0.80f), new Vector2(0.9f, -0.80f), num / 3);
                break;
            case 2: // circle
                int o = 0;
                for (float i = 0; i < num; i++)
                {
                    int type = (tentacles[0] && tentacles[1] ? (int)o % 2 : (tentacles[0] ? 0 : 1));
                    if (type == 1)
                        i += 0.75f;
                    GameObject obj = CreatePoint(0.86f * Mathf.Sin(i / num * 2 * Mathf.PI), 0.86f * Mathf.Cos(i / num * 2 * Mathf.PI), type);
                    obj.transform.localEulerAngles = new Vector3(0, 0, -i / num * 360f);
                    if(type == 1)
                        i += 0.75f;
                    o++;
                }
                break;          
            case 5:
                CreateLine(new Vector2(0.85f, 0.6f), new Vector2(0.4f, -0.7f), num / 4);
                CreateLine(new Vector2(0.4f, -0.65f), new Vector2(-0.9f, -0.65f), num / 4);
                CreateLine(new Vector2(-0.9f, -0.65f), new Vector2(-0.3f, 0.7f), num / 4);
                CreateLine(new Vector2(-0.3f, 0.6f), new Vector2(0.85f, 0.6f), num / 4);
                break;
        }
    }

    GameObject CreatePoint(float x, float y, int type)
    {
        GameObject obj = Instantiate(type == 0 ? GameManager.instance.legPrefab : GameManager.instance.bubblePrefab);
        obj.SetActive(true);
        obj.transform.SetParent(transform);
        obj.transform.localScale = new Vector3(1, 1, 1);
        obj.transform.localPosition = new Vector3(scale * x, scale * y, 0);
        legs.Add(obj.transform);
        return obj;
    }

    void CreateLine(Vector2 v1, Vector2 v2, int num)
    {
        Vector2 distance = v2 - v1;
        int o = 0;
        for (float i = 1; i < num; i++)
        {
            int type = (tentacles[0] && tentacles[1] ? (int)o % 2 : (tentacles[0] ? 0 : 1));
            if (type == 1)
                i += 0.75f;
            Vector2 point = v1 + (distance / num * i);            
            GameObject obj = CreatePoint(point.x, point.y, type);
            if (type == 1)
                i += 0.75f;

            Vector2 diff = point.normalized;
            float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
            obj.transform.localRotation = Quaternion.Euler(0f, 0f, rot_z - 90);
            //obj.transform.rotation = Quaternion.LookRotation(point);
            o++;
        }
    }

    public void PulseLegs()
    {
        for(int i = 0; i < legs.Count; i++)
        {
            if (legs[i].transform.eulerAngles.z != 0)
            {
                float pulseStr = 30;
                float rot = legs[i].transform.localEulerAngles.z < 0 ? legs[i].transform.localEulerAngles.z + 360 : legs[i].transform.localEulerAngles.z;
                bool direction = rot > 180;

                Sequence mySequence = DOTween.Sequence();
                mySequence.Append(legs[i].transform.DOLocalRotate(direction ? new Vector3(0, 0, -pulseStr) : new Vector3(0, 0, pulseStr), 0.1f).SetRelative())
                  .PrependInterval(0.05f)
                  .Append(legs[i].transform.DOLocalRotate(!direction ? new Vector3(0, 0, -pulseStr) : new Vector3(0, 0, pulseStr), 0.1f).SetRelative());                  
            }
                
        }
    }
      


}
