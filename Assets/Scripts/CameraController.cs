﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public float scale;
	void Update ()
    {      
        transform.position = new Vector3(Player.instance.transform.position.x * scale, Player.instance.transform.position.y * scale, transform.position.z);
	}
}
