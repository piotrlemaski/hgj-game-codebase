﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public enum MoveType { Hairs, Tail }

[System.Serializable]


public struct MoveStats
{
    public MoveType moveTypes;
    public float timeBeetweenMovingForward;
    public float movingSpeedPerSec;
    public float maxSpeed;
    public float fullRotationTime;
    public float drag;
};

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public List<MoveStats> moveStats = new List<MoveStats>();
    string[] shapeNames = new string[] {"kwadrat", "trojkat", "kolo", "obly", "trojkat_o", "romb", "romb_o" };
    public int patternLen;
    public int colorLen;
    public GameObject legPrefab;
    public GameObject bubblePrefab;
    public GameObject bodyParticlePrefab;
    public GameObject enemyPrefab;
    public List<Bacteria> bacterias = new List<Bacteria>();
    public GameObject paternPrefab;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        /*shape = new int[7] { 0, 1, 2, 3, 4, 5, 6 };
        edge = new int[6] { 0, 1, 2, 3, 4, 5 };
        pattern =  new int[6] { 0, 1, 2, 3, 4, 5 };
        color = new int [3] { 0, 1, 2 };*/
        Bacteria[] startBacteria = FindObjectsOfType<Bacteria>();
        for(int i = 0; i < startBacteria.Length; i++)
        {
            bacterias.Add(startBacteria[i]);
        }
    }

    public void DestroyBacteria(Bacteria Bacteria)
    {
        bacterias.Remove(Bacteria);
        Destroy(Bacteria.gameObject);
    }

    public Bacteria FindClosestBacteria(Transform t)
    {
        float min = 1000000;
        Bacteria closest = null;
        foreach (Bacteria bacteria in bacterias)
        { 
            float magnitude = (bacteria.transform.position - t.position).magnitude;
            if (t != bacteria.transform && min > magnitude)
            {
                closest = bacteria;
                min = magnitude;
            }
        }
        return closest;
    }

    public Sprite GetSprite(int shape, int color, bool[] tentacles)
    {
        return Resources.Load<Sprite>("obiekty/" + shapeNames[shape] + "_" + (color == 0 ? "b" : (color == 1 ? "c": "s")));
    }

    public Sprite GetPattern(int shape, int patern)
    {
        return Resources.Load<Sprite>("paski/" + shapeNames[shape] + "_" + (patern == 0 ? "c" : "g"));
    }

    public MoveStats GetMoveStatWithType(MoveType moveType)
    {
        return moveStats.Find(stat => stat.moveTypes == moveType);
    }

    public GameObject InstantiateObj(GameObject prefab, Transform parent, float scale)
    {
        GameObject obj = Instantiate(prefab);
        obj.SetActive(true);
        obj.transform.SetParent(parent);
        obj.transform.localScale = new Vector3(1, 1, 1);
        obj.transform.localPosition = new Vector3(scale, scale, 0);
        return obj;
    }

    /*public Sprite CompareSprites(Bacteria Bacteria)
    {
        Sprite playerSprite;
        playerSprite = player.GetComponent<Image>().sprite;
        return playerSprite;
    }*/
}
