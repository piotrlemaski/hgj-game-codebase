﻿using System;

namespace AssemblyCSharp
{
	public struct EnemyEntity
	{
		public string shape;
		public string color;
		public string sheathType;
		public string sheathMode;
		public string pattern;
	}
}

