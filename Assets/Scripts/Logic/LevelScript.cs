﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using LitJson;
using AssemblyCSharp;

public class LevelScript : MonoBehaviour
{
	private string jsonString;
	private JsonData levelData;


	[HideInInspector]
	public List<string> targetShapeList;
	public string targetColor;
	public string targetSheathType;
	public string targetSheathMode;
	public string targetPattern;

	private int enemyGenCount = 10;

	// Use this for initialization
	void Start ()
	{
		jsonString = File.ReadAllText (Application.dataPath + "/Configs/LevelConf.json");
		levelData = JsonMapper.ToObject (jsonString);

		for (int i = 0; i < levelData ["TargetTraits"] ["Shape"].Count; i++) 
		{
			targetShapeList.Add( levelData["TargetTraits"]["Shape"][i].ToString() );
		}

		targetColor = levelData ["TargetTraits"] ["Color"].ToString();

		targetSheathType = levelData ["TargetTraits"] ["Sheath"] [0].ToString ();
		targetSheathMode = levelData ["TargetTraits"] ["Sheath"] [1].ToString ();

		targetPattern = levelData ["TargetTraits"] ["Pattern"].ToString ();
	}

	EnemyEntity GenerateEnemy()
	{
		EnemyEntity output;

		int shapesNum = levelData ["EnemiesTraits"] ["Shape"].Count;
		output.shape = levelData ["EnemiesTraits"] ["Shape"] [Random.Range (0, shapesNum)].ToString();

		int colorsNum = levelData ["EnemiesTraits"] ["Color"].Count;
		output.color = levelData ["EnemiesTraits"] ["Color"] [Random.Range (0, colorsNum)].ToString();

		int sheathTypesNum = levelData ["EnemiesTraits"] ["Sheath"] ["Type"].Count;
		output.sheathType = levelData ["EnemiesTraits"] ["Sheath"] ["Type"] [Random.Range (0, sheathTypesNum)].ToString();

		int sheathModesNum = levelData ["EnemiesTraits"] ["Sheath"] ["Mode"].Count;
		output.sheathMode = levelData ["EnemiesTraits"] ["Sheath"] ["Mode"] [Random.Range (0, sheathModesNum)].ToString();

		int patternsNum = levelData ["EnemiesTraits"] ["Pattern"].Count;
		output.pattern = levelData ["EnemiesTraits"] ["Pattern"] [Random.Range (0, patternsNum)].ToString();

		return output;
	}

	void Update()
	{
	}
}
