﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	IEnumerator LoadLevel()
	{
		GetComponent<AudioSource> ().Play ();
		GetComponent<Fading> ().BeginFade (1);

		yield return new WaitForSeconds (0.6f);

		string nextLevelName = "game";
		if (Application.loadedLevelName == "menu") {
			nextLevelName = "game";
		} else if (Application.loadedLevelName == "game") {
			nextLevelName = "menu";
		} else if (Application.loadedLevelName == "splash") {
			nextLevelName = "menu";
		}

		SceneManager.LoadScene ( nextLevelName );
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("space")) 
		{
			StartCoroutine (LoadLevel());
		}
	}

	void LateUpdate() {
		
	}
}
