﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Enemy : Bacteria
{
    public float interceptPower = 1;
    public float attitude; //1 - attacking, 0 - no fucks given, -1 - timid
    Vector3 newRandomPos;
    float maxRange = 500;

    void Update()
    {
        Bacteria closest = GameManager.instance.FindClosestBacteria(transform);
        if(closest != null && attitude != 0 && Mathf.Abs(attitude) * maxRange > (closest.transform.localPosition - transform.localPosition).magnitude)
        {
            Vector3 positionToGo = closest.transform.localPosition;       
            Bacteria other = closest.GetComponent<Bacteria>();
            Debug.Log("closest " + (closest != null ? closest.name : "none"));                
            if(attitude > 0)
            {
                Vector3 posDiff = closest.transform.localPosition - transform.localPosition;
                Vector3 v = new Vector3(other.speed.x, other.speed.y, 0) * posDiff.magnitude / (speed.magnitude != 0 ? speed.magnitude : 0.0001f);
                positionToGo += v;
                GoTo(positionToGo, true);
            }
            else
            {
                positionToGo = -closest.transform.localPosition + transform.localPosition;
                GoTo(positionToGo, false);
            }                
            //Debug.Log(name + " " + positionToGo + " " + Mathf.Clamp((positionToGo.normalized - v.normalized).magnitude, 0, 1) + " " + closest.transform.localPosition + " " + positionToGo + " " + other.speed + " posDiff " + posDiff + " " +  posDiff.magnitude + " " + speed.magnitude);
        }
        else
        {
            float maxPos = 1000;
            if(newRandomPos == null || (transform.localPosition - newRandomPos).magnitude < 50)
                newRandomPos = new Vector3(Random.Range(-maxPos, maxPos), Random.Range(-maxPos, maxPos));
            GoTo(newRandomPos, true);
        }

        BakteriaUpdate();
    }
}
