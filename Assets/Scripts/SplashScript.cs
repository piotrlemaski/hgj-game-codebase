﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SplashScript : MonoBehaviour {
	IEnumerator LoadLevel()
	{
		GetComponent<Fading> ().BeginFade (1);

		yield return new WaitForSeconds (0.6f);

		SceneManager.LoadScene ("_Scenes/menu");
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("space")) 
		{
			StartCoroutine (LoadLevel());
		}
	}

	void LateUpdate() {

	}
}
