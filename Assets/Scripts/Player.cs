﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;


public class Player : Bacteria
{
    public static float a = 10;
    public static Player instance;

    void Awake()
    {
        instance = this;
    }

    void Update()
    {
        Vector2 mousePos = new Vector2(Input.mousePosition.x - Screen.width / 2, Input.mousePosition.y - Screen.height / 2);
        if (Input.GetMouseButton(0))
            GoTo(mousePos, false);
        BakteriaUpdate();
        if(tentacles[0] && tentacles[1] && shape == 2 && color == 1)
        {
            Application.LoadLevel("engame");
        }
    }


    void OnTriggerEnter2D(Collider2D other)
    {     
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
        {
            shape = CheckShape(enemy.shape + 1);
            Debug.Log("shape " + shape + " " + (enemy.shape + 1));
            shape--;
            tentacles = CheckEdge(enemy.tentacles);
            pattern = enemy.pattern;
            color = ChangeColor(enemy.color, color);
            Vector2 size = enemy.image.rectTransform.rect.size * 0.9f / 2;
            for (int i = 0; i < Random.Range(15, 30); i++)
            {
                GameObject obj = GameManager.instance.InstantiateObj(GameManager.instance.bodyParticlePrefab, enemy.transform, 1);
                Vector3 pos = new Vector3(Random.Range(-size.x, size.x), Random.Range(-size.y, size.y), 0);
                obj.transform.localPosition = pos;
                obj.transform.SetParent(enemy.transform.parent);
                obj.GetComponent<Rigidbody2D>().velocity = pos.normalized * 5;
                obj.GetComponent<Image>().sprite = GameManager.instance.GetSprite(enemy.shape, enemy.color, enemy.tentacles);
                Sequence mySequence = DOTween.Sequence();
                mySequence.PrependInterval(2f)
                    .Append(obj.transform.DOScale(0, 1f));
                Destroy(obj, 3f);
            }
            GameManager.instance.DestroyBacteria(enemy);
            Debug.Log("Collision");
            Refresh();
        }    
    }

    int ChangeColor(int enemyColor, int color)
    {
        if (enemyColor == color)
        {
            return color;
        }
        else if (enemyColor > color)
        {
            return color + 1;
        }
        else
        {
            return color - 1;
        }
    }

    int ChangePattern(int enemyPattern, int pattern)
    {
        if (enemyPattern == pattern)
        {
            return pattern;
        }
        else if (enemyPattern > pattern)
        {
            return pattern + 1;
        }
        else
        {
            return pattern - 1;
        }
    }


    int CheckShape(int enemyShape)
    {
        shape++;
        Debug.Log("CheckShape " + shape + " " + enemyShape);        
        if (shape == 1)
        {
            if (enemyShape == 1) return 1;
            else if (enemyShape == 2) return 6;
            else if (enemyShape == 3) return 4;
        }
        else if (shape == 2)
        {
            if (enemyShape == 1) return 6;
            else if (enemyShape == 2) return 2;
            else if (enemyShape == 3) return 5;
        }
        else if (shape == 3)
        {
            if (enemyShape == 1) return 4;
            else if (enemyShape == 2) return 5;
            else if (enemyShape == 3) return 3;
        }
        else if (shape == 4)
        {
            if (enemyShape == 1) return 1;
            else if (enemyShape == 2) return 7;
            else if (enemyShape == 3) return 3;
        }
        else if (shape == 5)
        {
            if (enemyShape == 1) return 7;
            else if (enemyShape == 2) return 2;
            else if (enemyShape == 3) return 3;
        }
        else if (shape == 6)
        {
            if (enemyShape == 1) return 1;
            else if (enemyShape == 2) return 2;
            else if (enemyShape == 3) return 7;
        }
        else if (shape == 7)
        {
            if (enemyShape == 1) return 1;
            else if (enemyShape == 2) return 2;
            else if (enemyShape == 3) return 3;
        }
        return 1;
    }

    bool [] CheckEdge(bool [] enemyTentacles)
    {
        bool[] returnArray = new bool[2];
        Debug.Log("CheckEdge " + tentacles.Length);
        if (tentacles[0] == false && tentacles[1] == false)
        {
            if (enemyTentacles[0] == true && enemyTentacles[1] == false)
            {
                returnArray[0] = true;
                return returnArray;
            }
            else if (enemyTentacles[0] == false && enemyTentacles[1] == true)
            {
                returnArray[1] = true;
                return returnArray;
            }
            else if (enemyTentacles[0] == false && enemyTentacles[1] == false)
            {
                returnArray[0] = false;
                returnArray[1] = false;
                return returnArray;
            }
        }
        else if (tentacles[0] == true && tentacles[1] == false)
        {
            if (enemyTentacles[0] == true && enemyTentacles[1] == false)
            {
                return returnArray;
            }
            else if (enemyTentacles[0] == false && enemyTentacles[1] == true)
            {
                returnArray[1] = true;
                return returnArray;
            }
            else if ((enemyTentacles[0] == false && enemyTentacles[1] == false))
            {
                returnArray[0] = false;
                returnArray[1] = false;
                return returnArray;
            }
        }
        else if (tentacles[0] == false && tentacles[1] == true)
        {
            if (enemyTentacles[0] == true && enemyTentacles[1] == false)
            {
                returnArray[0] = true;
                return returnArray;
            }
            else if (enemyTentacles[0] == false && enemyTentacles[1] == true)
            {
                return returnArray;
            }
            else if ((enemyTentacles[0] == false && enemyTentacles[1] == false))
            {
                returnArray[0] = false;
                returnArray[1] = false;
                return returnArray;
            }
        }
        else if (tentacles[0] == true && tentacles[1] == true)
        {
            if (enemyTentacles[0] == true && enemyTentacles[1] == false)
            {
                returnArray[1] = false;
                return returnArray;
            }
            else if (enemyTentacles[0] == false && enemyTentacles[1] == true)
            {
                returnArray[0] = false;
                return returnArray;
            }
            else if ((enemyTentacles[0] == false && enemyTentacles[1] == false))
            {
                returnArray[0] = false;
                returnArray[1] = false;
                return returnArray;
            }
        }
        return returnArray;
        }
    }